# FILE VIEW ACCESS

## CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## INTRODUCTION

The File View Access module helps to define the permission for the enduser to
view the uploaded files or not. The uploaders can decide whether the uploaded
file needs to be accessed by a particular set of users.

The privilege is assigned for each file that is uploaded by the user. This
module was developed to overcome the problem with the private file system
for specific file uploads rather than all.

 - For a full description of the module visit:
   <https://www.drupal.org/project/file_view_access>

 - To submit bug reports and feature suggestions, or to track changes visit:
   <https://www.drupal.org/project/issues/file_view_access>

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

 - Install the File View Access module as you would normally install a
   contributed Drupal module. Visit <https://www.drupal.org/node/1897420> for
   further information.

## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Enable the permission for roles to access the view access files at
       /admin/people/permissions#module-file_view_access
    3. Enable file view access on required file upload fields.
    4. During content creation define the files that have option to "file view
       access".
    5. In display file will render for selected role only.

## MAINTAINERS

Support by volunteers is available on

 - <http://drupal.org/project/issues/acl?status=All&version=8.x>

   Please consider helping others as a way to give something back to the community
   that provides Drupal and the contributed modules to you free of charge.

   For paid support and customizations of this module or other Drupal work,
   contact the maintainer through his contact form:

 - Arunkumar Kuppuswamy (arunkumark) - <http://drupal.org/u/arunkumark>

Acknowledgements:

 - This module developed based on the option in Private access in Drupal 6
   module.
