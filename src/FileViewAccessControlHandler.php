<?php

namespace Drupal\file_view_access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\file\FileAccessFormatterControlHandlerInterface;
use Drupal\file\FileAccessControlHandler;
use Drupal\Core\StreamWrapper\StreamWrapperManager;

/**
 * Provides a File access control handler.
 */
class FileViewAccessControlHandler extends FileAccessControlHandler implements FileAccessFormatterControlHandlerInterface {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    // Check if the file permission exists or not.
    if ($operation == 'view') {
      // Check if view access is enabled.
      $view_access = $entity->get('view_access')->value;
      if (is_numeric($view_access) && $view_access) {
        if (StreamWrapperManager::getScheme($entity->getFileUri()) === 'public') {
          return AccessResult::allowedIfHasPermission($account, 'file view access');
        }
        // Handle the case where the file scheme is not 'public'.
        // You might want to return a different AccessResult here.
        // Example: return AccessResult::forbidden('File scheme is not public.');
      }
      // If view access is not enabled, fall back to the parent check.
      return parent::checkAccess($entity, $operation, $account);
    }

    // For other operations, fall back to the parent check.
    return parent::checkAccess($entity, $operation, $account);
  }

}
